/*******************************************************************************
 * Copyright 2013 Comcast Cable Communications Management, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.example.dev.layouts.FreeFlow.ArtBook.models;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.dev.layouts.FreeFlow.ArtBook.ArtbookActivity;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import android.os.AsyncTask;
import android.util.Log;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

//注释掉的代码是老版本的okHttp的写法！！！

public class DribbbleFetch {

    public static final String TAG = "DribbbleFetch";

    public void load(final ArtbookActivity caller, final int itemsPerPage, final int page) {

        new AsyncTask<String, Void, String>() {

            OkHttpClient client = new OkHttpClient();

            protected String doInBackground(String... urls) {
                try {
//					return get(new URL(urls[0]));

                    return run("http://api.dribbble.com/shots/popular?per_page=" + itemsPerPage + "&page=" + page);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            protected void onPostExecute(String data) {
                DribbbleFeed feed = new Gson().fromJson(data, DribbbleFeed.class);
                caller.onDataLoaded(feed);
            }

//			String get(URL url) throws IOException {
//				HttpURLConnection connection = client.open(url);
//				InputStream in = null;
//				try {
//					in = connection.getInputStream();
//					byte[] response = readFully(in);
//					return new String(response, "UTF-8");
//				} finally {
//					if (in != null)
//						in.close();
//				}
//			}

            String run(String url) throws IOException {
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                return response.body().string();
            }

//			byte[] readFully(InputStream in) throws IOException {
//				ByteArrayOutputStream out = new ByteArrayOutputStream();
//				byte[] buffer = new byte[1024];
//				for (int count; (count = in.read(buffer)) != -1;) {
//					out.write(buffer, 0, count);
//				}
//				return out.toByteArray();
//			}

        }.execute();
    }

}
