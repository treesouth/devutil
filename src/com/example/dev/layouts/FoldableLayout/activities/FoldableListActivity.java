package com.example.dev.layouts.FoldableLayout.activities;

import android.os.Bundle;
import com.example.dev.R;
import com.example.dev.layouts.FoldableLayout.items.PaintingsAdapter;
import com.example.dev.layouts_library.FoldableLayout.FoldableListLayout;
import com.example.dev.layouts_library.FoldableLayout.Views;

public class FoldableListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layouts_foldablelayout_activity_foldable_list);

        FoldableListLayout foldableListLayout = Views.find(this, R.id.foldable_list);
        foldableListLayout.setAdapter(new PaintingsAdapter(this));
    }

}
