package com.example.dev.layouts.AKParallax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.dev.R;

public class AKParallaxActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layouts_akparallax_activity_main);

        Button scrollViewParallaxButton = (Button) findViewById(R.id.button_scrollview_parallax);
        Button listViewParallaxButton = (Button) findViewById(R.id.button_listview_parallax);

        scrollViewParallaxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AKParallaxActivity.this, ParallaxScrollViewActivity.class);
                startActivity(i);
            }
        });
        listViewParallaxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AKParallaxActivity.this, ParallaxListViewActivity.class);
                startActivity(i);
            }
        });

    }

}
