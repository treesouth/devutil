package com.example.dev;

/**
 * Created by zn on 14-6-25.
 */
public class Const {
    public static final int WIDGET = 0;
    public static final int LISTGRID = 1;
    public static final int POPUPS = 2;
    public static final int MENUS = 3;
    public static final int GRAPHIC = 4;
    public static final int ACTIONBAR = 5;
    public static final int UTILS = 6;
    public static final int ANIMATION = 7;
    public static final int TABS = 8;
    public static final int LAYOUTS = 9;
    public static final int IMITATE = 10;
    public static final int OTHER = 11;

}
