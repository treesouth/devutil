package com.example.dev.widget_library.CircleProgressButton;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
