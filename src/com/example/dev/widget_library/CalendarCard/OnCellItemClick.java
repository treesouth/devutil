package com.example.dev.widget_library.CalendarCard;

import android.view.View;

public interface OnCellItemClick {
	
	public void onCellClick(View v, CardGridItem item);

}
