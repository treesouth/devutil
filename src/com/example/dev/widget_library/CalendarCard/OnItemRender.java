package com.example.dev.widget_library.CalendarCard;


public interface OnItemRender {
	
	public void onRender(CheckableLayout v, CardGridItem item);

}
