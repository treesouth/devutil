/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.example.dev.animation_library.EasingFunctions;

import com.example.dev.animation_library.EasingFunctions.back.BackEaseIn;
import com.example.dev.animation_library.EasingFunctions.back.BackEaseInOut;
import com.example.dev.animation_library.EasingFunctions.back.BackEaseOut;
import com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseIn;
import com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseInOut;
import com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseOut;
import com.example.dev.animation_library.EasingFunctions.circ.CircEaseIn;
import com.example.dev.animation_library.EasingFunctions.circ.CircEaseInOut;
import com.example.dev.animation_library.EasingFunctions.circ.CircEaseOut;
import com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseIn;
import com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseInOut;
import com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseOut;
import com.example.dev.animation_library.EasingFunctions.elastic.ElasticEaseIn;
import com.example.dev.animation_library.EasingFunctions.elastic.ElasticEaseOut;
import com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseIn;
import com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseInOut;
import com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseOut;
import com.example.dev.animation_library.EasingFunctions.linear.Linear;
import com.example.dev.animation_library.EasingFunctions.quad.QuadEaseIn;
import com.example.dev.animation_library.EasingFunctions.quad.QuadEaseInOut;
import com.example.dev.animation_library.EasingFunctions.quad.QuadEaseOut;
import com.example.dev.animation_library.EasingFunctions.quint.QuintEaseIn;
import com.example.dev.animation_library.EasingFunctions.quint.QuintEaseInOut;
import com.example.dev.animation_library.EasingFunctions.quint.QuintEaseOut;
import com.example.dev.animation_library.EasingFunctions.sine.SineEaseIn;
import com.example.dev.animation_library.EasingFunctions.sine.SineEaseInOut;
import com.example.dev.animation_library.EasingFunctions.sine.SineEaseOut;

public enum  Skill {

    BackEaseIn(com.example.dev.animation_library.EasingFunctions.back.BackEaseIn.class),
    BackEaseOut(com.example.dev.animation_library.EasingFunctions.back.BackEaseOut.class),
    BackEaseInOut(com.example.dev.animation_library.EasingFunctions.back.BackEaseInOut.class),

    BounceEaseIn(com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseIn.class),
    BounceEaseOut(com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseOut.class),
    BounceEaseInOut(com.example.dev.animation_library.EasingFunctions.bounce.BounceEaseInOut.class),

    CircEaseIn(com.example.dev.animation_library.EasingFunctions.circ.CircEaseIn.class),
    CircEaseOut(com.example.dev.animation_library.EasingFunctions.circ.CircEaseOut.class),
    CircEaseInOut(com.example.dev.animation_library.EasingFunctions.circ.CircEaseInOut.class),

    CubicEaseIn(com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseIn.class),
    CubicEaseOut(com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseOut.class),
    CubicEaseInOut(com.example.dev.animation_library.EasingFunctions.cubic.CubicEaseInOut.class),

    ElasticEaseIn(com.example.dev.animation_library.EasingFunctions.elastic.ElasticEaseIn.class),
    ElasticEaseOut(com.example.dev.animation_library.EasingFunctions.elastic.ElasticEaseOut.class),

    ExpoEaseIn(com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseIn.class),
    ExpoEaseOut(com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseOut.class),
    ExpoEaseInOut(com.example.dev.animation_library.EasingFunctions.expo.ExpoEaseInOut.class),

    QuadEaseIn(com.example.dev.animation_library.EasingFunctions.quad.QuadEaseIn.class),
    QuadEaseOut(com.example.dev.animation_library.EasingFunctions.quad.QuadEaseOut.class),
    QuadEaseInOut(com.example.dev.animation_library.EasingFunctions.quad.QuadEaseInOut.class),

    QuintEaseIn(com.example.dev.animation_library.EasingFunctions.quint.QuintEaseIn.class),
    QuintEaseOut(com.example.dev.animation_library.EasingFunctions.quint.QuintEaseOut.class),
    QuintEaseInOut(com.example.dev.animation_library.EasingFunctions.quint.QuintEaseInOut.class),

    SineEaseIn(com.example.dev.animation_library.EasingFunctions.sine.SineEaseIn.class),
    SineEaseOut(com.example.dev.animation_library.EasingFunctions.sine.SineEaseOut.class),
    SineEaseInOut(com.example.dev.animation_library.EasingFunctions.sine.SineEaseInOut.class),

    Linear(com.example.dev.animation_library.EasingFunctions.linear.Linear.class);


    private Class easingMethod;

    private Skill(Class clazz) {
        easingMethod = clazz;
    }

    public BaseEasingMethod getMethod(float duration) {
        try {
            return (BaseEasingMethod)easingMethod.getConstructor(float.class).newInstance(duration);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Can not init easingMethod instance");
        }
    }
}
