package com.example.dev.animation_library.ParallaxPager;

public class ParallaxViewTag {
  protected int index;
  protected float xIn;
  protected float xOut;
  protected float yIn;
  protected float yOut;
  protected float alphaIn;
  protected float alphaOut;
}
