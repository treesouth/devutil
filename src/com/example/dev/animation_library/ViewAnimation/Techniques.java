
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.example.dev.animation_library.ViewAnimation;

import com.example.dev.animation_library.ViewAnimation.attention.BounceAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.FlashAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.PulseAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.RubberBandAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.ShakeAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.StandUpAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.SwingAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.TadaAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.WaveAnimator;
import com.example.dev.animation_library.ViewAnimation.attention.WobbleAnimator;
import com.example.dev.animation_library.ViewAnimation.bouncing_entrances.BounceInAnimator;
import com.example.dev.animation_library.ViewAnimation.bouncing_entrances.BounceInDownAnimator;
import com.example.dev.animation_library.ViewAnimation.bouncing_entrances.BounceInLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.bouncing_entrances.BounceInRightAnimator;
import com.example.dev.animation_library.ViewAnimation.bouncing_entrances.BounceInUpAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_entrances.FadeInAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_entrances.FadeInDownAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_entrances.FadeInLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_entrances.FadeInRightAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_entrances.FadeInUpAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_exits.FadeOutAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_exits.FadeOutDownAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_exits.FadeOutLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_exits.FadeOutRightAnimator;
import com.example.dev.animation_library.ViewAnimation.fading_exits.FadeOutUpAnimator;
import com.example.dev.animation_library.ViewAnimation.flippers.FlipInXAnimator;
import com.example.dev.animation_library.ViewAnimation.flippers.FlipOutXAnimator;
import com.example.dev.animation_library.ViewAnimation.flippers.FlipOutYAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_entrances.RotateInAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_entrances.RotateInDownLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_entrances.RotateInDownRightAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_entrances.RotateInUpLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_entrances.RotateInUpRightAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_exits.RotateOutAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_exits.RotateOutDownLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_exits.RotateOutDownRightAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_exits.RotateOutUpLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.rotating_exits.RotateOutUpRightAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideInDownAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideInLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideInRightAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideInUpAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideOutDownAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideOutLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideOutRightAnimator;
import com.example.dev.animation_library.ViewAnimation.sliders.SlideOutUpAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.HingeAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.RollInAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.RollOutAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.in.DropOutAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.in.LandingAnimator;
import com.example.dev.animation_library.ViewAnimation.specials.out.TakingOffAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_entrances.ZoomInAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_entrances.ZoomInDownAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_entrances.ZoomInLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_entrances.ZoomInRightAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_entrances.ZoomInUpAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_exits.ZoomOutAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_exits.ZoomOutDownAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_exits.ZoomOutLeftAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_exits.ZoomOutRightAnimator;
import com.example.dev.animation_library.ViewAnimation.zooming_exits.ZoomOutUpAnimator;

public enum Techniques {

    DropOut(DropOutAnimator.class),
    Landing(LandingAnimator.class),
    TakingOff(TakingOffAnimator.class),

    Flash(FlashAnimator.class),
    Pulse(PulseAnimator.class),
    RubberBand(RubberBandAnimator.class),
    Shake(ShakeAnimator.class),
    Swing(SwingAnimator.class),
    Wobble(WobbleAnimator.class),
    Bounce(BounceAnimator.class),
    Tada(TadaAnimator.class),
    StandUp(StandUpAnimator.class),
    Wave(WaveAnimator.class),

    Hinge(HingeAnimator.class),
    RollIn(RollInAnimator.class),
    RollOut(RollOutAnimator.class),

    BounceIn(BounceInAnimator.class),
    BounceInDown(BounceInDownAnimator.class),
    BounceInLeft(BounceInLeftAnimator.class),
    BounceInRight(BounceInRightAnimator.class),
    BounceInUp(BounceInUpAnimator.class),

    FadeIn(FadeInAnimator.class),
    FadeInUp(FadeInUpAnimator.class),
    FadeInDown(FadeInDownAnimator.class),
    FadeInLeft(FadeInLeftAnimator.class),
    FadeInRight(FadeInRightAnimator.class),

    FadeOut(FadeOutAnimator.class),
    FadeOutDown(FadeOutDownAnimator.class),
    FadeOutLeft(FadeOutLeftAnimator.class),
    FadeOutRight(FadeOutRightAnimator.class),
    FadeOutUp(FadeOutUpAnimator.class),

    FlipInX(FlipInXAnimator.class),
    FlipOutX(FlipOutXAnimator.class),

    FlipOutY(FlipOutYAnimator.class),
    RotateIn(RotateInAnimator.class),
    RotateInDownLeft(RotateInDownLeftAnimator.class),
    RotateInDownRight(RotateInDownRightAnimator.class),
    RotateInUpLeft(RotateInUpLeftAnimator.class),
    RotateInUpRight(RotateInUpRightAnimator.class),

    RotateOut(RotateOutAnimator.class),
    RotateOutDownLeft(RotateOutDownLeftAnimator.class),
    RotateOutDownRight(RotateOutDownRightAnimator.class),
    RotateOutUpLeft(RotateOutUpLeftAnimator.class),
    RotateOutUpRight(RotateOutUpRightAnimator.class),

    SlideInLeft(SlideInLeftAnimator.class),
    SlideInRight(SlideInRightAnimator.class),
    SlideInUp(SlideInUpAnimator.class),
    SlideInDown(SlideInDownAnimator.class),

    SlideOutLeft(SlideOutLeftAnimator.class),
    SlideOutRight(SlideOutRightAnimator.class),
    SlideOutUp(SlideOutUpAnimator.class),
    SlideOutDown(SlideOutDownAnimator.class),

    ZoomIn(ZoomInAnimator.class),
    ZoomInDown(ZoomInDownAnimator.class),
    ZoomInLeft(ZoomInLeftAnimator.class),
    ZoomInRight(ZoomInRightAnimator.class),
    ZoomInUp(ZoomInUpAnimator.class),

    ZoomOut(ZoomOutAnimator.class),
    ZoomOutDown(ZoomOutDownAnimator.class),
    ZoomOutLeft(ZoomOutLeftAnimator.class),
    ZoomOutRight(ZoomOutRightAnimator.class),
    ZoomOutUp(ZoomOutUpAnimator.class);



    private Class animatorClazz;

    private Techniques(Class clazz) {
        animatorClazz = clazz;
    }

    public BaseViewAnimator getAnimator() {
        try {
            return (BaseViewAnimator) animatorClazz.newInstance();
        } catch (Exception e) {
            throw new Error("Can not init animatorClazz instance");
        }
    }
}
