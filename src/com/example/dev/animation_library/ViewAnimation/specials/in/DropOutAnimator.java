package com.example.dev.animation_library.ViewAnimation.specials.in;

import android.view.View;

import com.example.dev.animation_library.EasingFunctions.Glider;
import com.example.dev.animation_library.EasingFunctions.Skill;
import com.example.dev.animation_library.ViewAnimation.BaseViewAnimator;
import com.nineoldandroids.animation.ObjectAnimator;

public class DropOutAnimator extends BaseViewAnimator{
    @Override
    protected void prepare(View target) {
        int distance = target.getTop() + target.getHeight();
        getAnimatorAgent().playTogether(
                ObjectAnimator.ofFloat(target, "alpha", 0, 1),
                Glider.glide(Skill.BounceEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "translationY", -distance, 0))
        );
    }
}
