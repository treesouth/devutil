package com.example.dev.animation_library.ViewAnimation.specials.in;

import android.view.View;

import com.example.dev.animation_library.EasingFunctions.Glider;
import com.example.dev.animation_library.EasingFunctions.Skill;
import com.example.dev.animation_library.ViewAnimation.BaseViewAnimator;
import com.nineoldandroids.animation.ObjectAnimator;

public class LandingAnimator extends BaseViewAnimator{
    @Override
    protected void prepare(View target) {
        getAnimatorAgent().playTogether(
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "scaleX", 1.5f, 1f)),
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "scaleY", 1.5f, 1f)),
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "alpha", 0, 1f))
        );
    }
}
