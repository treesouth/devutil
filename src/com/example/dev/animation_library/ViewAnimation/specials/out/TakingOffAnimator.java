package com.example.dev.animation_library.ViewAnimation.specials.out;

import android.view.View;

import com.example.dev.animation_library.EasingFunctions.Glider;
import com.example.dev.animation_library.EasingFunctions.Skill;
import com.example.dev.animation_library.ViewAnimation.BaseViewAnimator;
import com.nineoldandroids.animation.ObjectAnimator;

public class TakingOffAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(View target) {
        getAnimatorAgent().playTogether(
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "scaleX", 1f, 1.5f)),
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "scaleY", 1f, 1.5f)),
                Glider.glide(Skill.QuintEaseOut, getDuration(), ObjectAnimator.ofFloat(target, "alpha", 1, 0))
        );
    }
}
