package com.example.dev.imitate.i4;

import android.app.Activity;
import android.os.Bundle;
import com.example.dev.R;
import com.example.dev.imitate.i4.lib.CoverManager;

public class BigDemoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imitate_i4_activity_big);

        getActionBar().hide();
        CoverManager.getInstance().init(this);

        CoverManager.getInstance().setMaxDragDistance(350);
        CoverManager.getInstance().setExplosionTime(150);
    }

}
