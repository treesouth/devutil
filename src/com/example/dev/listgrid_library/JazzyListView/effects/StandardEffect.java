package com.example.dev.listgrid_library.JazzyListView.effects;

import android.view.View;

import com.example.dev.listgrid_library.JazzyListView.JazzyEffect;
import com.nineoldandroids.view.ViewPropertyAnimator;

public class StandardEffect implements JazzyEffect {

    @Override
    public void initView(View item, int position, int scrollDirection) {
        // no op
    }

    @Override
    public void setupAnimation(View item, int position, int scrollDirection, ViewPropertyAnimator animator) {
        // no op
    }

}
