package com.example.dev.listgrid_library.ListViewAnimations;

import android.widget.AbsListView;

public interface ListViewSetter {

    void setAbsListView(AbsListView listView);
}
