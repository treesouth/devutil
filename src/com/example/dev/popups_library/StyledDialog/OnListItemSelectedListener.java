package com.example.dev.popups_library.StyledDialog;

/**
 *
 */
public interface OnListItemSelectedListener {

    public void onListItemSelected(String value, int number);
}
