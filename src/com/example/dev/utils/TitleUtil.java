package com.example.dev.utils;

import android.content.Context;
import com.example.dev.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zn on 14-6-26.
 */
public class TitleUtil {

    public static List<String> addTitleList(Context context, int type) {
        List<String> list = new ArrayList<>();
        switch (type) {
            case 0:
//                widget
                list.add(context.getString(R.string.widget_1));
                list.add(context.getString(R.string.widget_2));
                list.add(context.getString(R.string.widget_3));
                list.add(context.getString(R.string.widget_4));
                list.add(context.getString(R.string.widget_5));
                list.add(context.getString(R.string.widget_6));
                list.add(context.getString(R.string.widget_7));
                list.add(context.getString(R.string.widget_8));
                list.add(context.getString(R.string.widget_9));
                list.add(context.getString(R.string.widget_10));
                list.add(context.getString(R.string.widget_11));
                list.add(context.getString(R.string.widget_12));
                list.add(context.getString(R.string.widget_13));
                list.add(context.getString(R.string.widget_14));
                break;
            case 1:
//                list/grid
                list.add(context.getString(R.string.listgrid_1));
                list.add(context.getString(R.string.listgrid_2));
                list.add(context.getString(R.string.listgrid_3));
                list.add(context.getString(R.string.listgrid_4));
                list.add(context.getString(R.string.listgrid_5));
                list.add(context.getString(R.string.listgrid_6));
                list.add(context.getString(R.string.listgrid_7));
                list.add(context.getString(R.string.listgrid_8));
                list.add(context.getString(R.string.listgrid_9));
                list.add(context.getString(R.string.listgrid_10));
                break;
            case 2:
//                popups
                list.add(context.getString(R.string.popups_1));
                list.add(context.getString(R.string.popups_2));
                list.add(context.getString(R.string.popups_3));
                list.add(context.getString(R.string.popups_4));
                break;
            case 3:
//                menus
                list.add(context.getString(R.string.menus_1));
                list.add(context.getString(R.string.menus_2));
                break;
            case 4:
//                graphics
                list.add(context.getString(R.string.graphics_1));
                list.add(context.getString(R.string.graphics_2));
                list.add(context.getString(R.string.graphics_3));
                list.add(context.getString(R.string.graphics_4));
                list.add(context.getString(R.string.graphics_5));
                list.add(context.getString(R.string.graphics_6));
                list.add(context.getString(R.string.graphics_7));
                list.add(context.getString(R.string.graphics_8));
                list.add(context.getString(R.string.graphics_9));
                break;
            case 5:
//                actionbar
                list.add(context.getString(R.string.actionbar_1));
                list.add(context.getString(R.string.actionbar_2));
                break;
            case 6:
//                utils
                list.add(context.getString(R.string.utils_1));
                list.add(context.getString(R.string.utils_2));
                list.add(context.getString(R.string.utils_3));
                list.add(context.getString(R.string.utils_4));
                list.add(context.getString(R.string.utils_5));
                list.add(context.getString(R.string.utils_6));
                break;
            case 7:
//                animation
                list.add(context.getString(R.string.aniamtion_1));
                list.add(context.getString(R.string.aniamtion_2));
                list.add(context.getString(R.string.aniamtion_3));
                list.add(context.getString(R.string.aniamtion_4));
                list.add(context.getString(R.string.aniamtion_5));
                list.add(context.getString(R.string.aniamtion_6));
                list.add(context.getString(R.string.aniamtion_7));
                list.add(context.getString(R.string.aniamtion_8));
                list.add(context.getString(R.string.aniamtion_9));
                list.add(context.getString(R.string.aniamtion_10));
                list.add(context.getString(R.string.aniamtion_11));
                list.add(context.getString(R.string.aniamtion_12));
                break;
            case 8:
//                tabs
                list.add(context.getString(R.string.tabs_1));
                list.add(context.getString(R.string.tabs_2));
                list.add(context.getString(R.string.tabs_3));
                list.add(context.getString(R.string.tabs_4));
                break;
            case 9:
//                layouts
                list.add(context.getString(R.string.layouts_1));
                list.add(context.getString(R.string.layouts_2));
                list.add(context.getString(R.string.layouts_3));
                list.add(context.getString(R.string.layouts_4));
                list.add(context.getString(R.string.layouts_5));
                list.add(context.getString(R.string.layouts_6));
                list.add(context.getString(R.string.layouts_7));
                list.add(context.getString(R.string.layouts_8));
                list.add(context.getString(R.string.layouts_9));
                list.add(context.getString(R.string.layouts_10));
                list.add(context.getString(R.string.layouts_11));
                break;
            case 10:
//                imitate
                list.add(context.getString(R.string.imitate_1));
                list.add(context.getString(R.string.imitate_2));
                list.add(context.getString(R.string.imitate_3));
                list.add(context.getString(R.string.imitate_4));
                break;
            case 11:
//                other
                list.add(context.getString(R.string.other_1));
                list.add(context.getString(R.string.other_2));
                list.add(context.getString(R.string.other_3));
                list.add(context.getString(R.string.other_4));
                list.add(context.getString(R.string.other_5));

                break;
        }
        return list;
    }

}
