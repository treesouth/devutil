package com.example.dev.utils;

import android.util.Log;

/**
 * Created by zn on 14-8-3.
 */
public class LogUtil {
    private static boolean debug = true;

    private LogUtil() {
    }

    private static class SingletonHolder {
        private static final LogUtil INSTANCE = new LogUtil();
    }

    // 延迟加载instance
    public static final LogUtil getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static void setDebug(boolean flag) {
        debug = flag;
    }

    private String getMethodName() {
        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        if (sts == null) {
            return null;
        }
        for (StackTraceElement st : sts) {
            if (st.isNativeMethod()) {
                continue;
            }
            if (st.getClassName().equals(Thread.class.getName())) {
                continue;
            }
            if (st.getClassName().equals(this.getClass().getName())) {
                continue;
            }
            return "[" + st.getMethodName() + "]" + " (" + st.getLineNumber() + ") ";
        }
        return null;
    }

    private static String createMessage() {
        String methodName = LogUtil.getInstance().getMethodName();
        return methodName == null ? "Log Message = null" : methodName;
    }

    public static void d(String tag, String message) {
        if (debug) {
            Log.d(tag, message);
        }
    }

    public static void d(Object cls) {
        if (debug) {
            d(cls.getClass().getSimpleName(), createMessage());
        }
    }

    public static void d(Object cls, String info) {
        if (debug) {
            d(cls.getClass().getSimpleName(), createMessage() + info);
        }
    }

    public static void i(String tag, String message) {
        if (debug) {
            Log.i(tag, message);
        }
    }

    public static void i(Object cls) {
        if (debug) {
            i(cls.getClass().getSimpleName(), createMessage());
        }
    }

    public static void i(Object cls, String info) {
        if (debug) {
            i(cls.getClass().getSimpleName(), createMessage() + info);
        }
    }

    public static void w(String tag, String message) {
        if (debug) {
            Log.w(tag, message);
        }
    }

    public static void w(Object cls) {
        if (debug) {
            w(cls.getClass().getSimpleName(), createMessage());
        }
    }

    public static void w(Object cls, String info) {
        if (debug) {
            w(cls.getClass().getSimpleName(), createMessage() + info);
        }
    }

    public static void e(String tag, String message) {
        if (debug) {
            Log.e(tag, message);
        }
    }

    public static void e(Object cls) {
        if (debug) {
            e(cls.getClass().getSimpleName(), createMessage());
        }
    }

    public static void e(Object cls, String info) {
        if (debug) {
            e(cls.getClass().getSimpleName(), createMessage() + info);
        }
    }

}
