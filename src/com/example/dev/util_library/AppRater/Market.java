package com.example.dev.util_library.AppRater;

import android.content.Context;
import android.net.Uri;

public interface Market {
    public Uri getMarketURI(Context context);
}
