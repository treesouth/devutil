package com.example.dev.other.o1;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import com.example.dev.R;

/**
 * @author manymore13
 * @Blog http://blog.csdn.net/manymore13
 */
public class PinchActivity extends Activity implements View.OnClickListener {

	private final static String TAG = "PinchActivity";
	
	// 屏幕宽度
	private int screentWidth = 0;
	
	// View可伸展最长的宽度
	private int maxWidth;
	
	// View可伸展最小宽度
	private int minWidth;
	
	// 当前点击的View
	private View currentView;
	
	// 显示最长的那个View
	private View preView;
	
	// 主布局ViewGroup
	private LinearLayout mainContain;
	
	// 标识符 动画是否结束
	private boolean animationIsEnd = true;
	
	// 变大操作
	private static final int OPE_BIG = 1;
	
	// 变小操作
	private static final int OPE_SMALL = 2;
	
	// 当前操作 -1表示无效操作
	private int currentOpe = -1;
	
	// 前进的步伐距离
	private static final int STEP = 10;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.other_o1_activity_main);

		initCommonData();
		
		initViewData();
		
		measureWidth(screentWidth); 

	}

	private void initViewData() {

		mainContain = (LinearLayout) this.findViewById(R.id.main_contain);
		View child;
		int childCount = mainContain.getChildCount();
		for (int i = 0; i < childCount; i++) {
			child = mainContain.getChildAt(i);
			child.setOnClickListener(this);
		}
	}
	
	private void initCommonData()
	{
		DisplayMetrics metric = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metric);
		screentWidth = metric.widthPixels; // 屏幕宽度（像素）
	}


	private void setCurrentViewParams() {

		if (currentView == null) {
			return;
		}
		LayoutParams params = currentView.getLayoutParams();
		if (params == null) {
			return;
		}
		int realWidth = params.width;
		int nextWidth = 0;
		if (currentOpe == OPE_BIG) {
			nextWidth = realWidth + STEP;
		} else if (currentOpe == OPE_SMALL) {
			nextWidth = realWidth - STEP;
		}
		if (nextWidth > maxWidth) {
			nextWidth = maxWidth;
		} else if (nextWidth < minWidth) {
			nextWidth = minWidth;
		}
		params.width = nextWidth;
		currentView.setLayoutParams(params);
		if (nextWidth == maxWidth || nextWidth == minWidth) {
			animationIsEnd = true;
			onOffClickable();
			stopAnimation();
			return;
		}
		mHandler.sendEmptyMessageDelayed(1, 20);
	}

	// 初始化宽度 测量max min 长度
	private void measureWidth(int screenWidth) {
		
		int halfWidth = screenWidth / 2;
		maxWidth = halfWidth - 50;
		minWidth = (screenWidth - maxWidth) / (mainContain.getChildCount() - 1);

		View child;
		int childCount = mainContain.getChildCount();
		for (int i = 0; i < childCount; i++) {
			child = mainContain.getChildAt(i);
			LayoutParams params = child.getLayoutParams();
			if (i == 0) {
				preView = child;
				params.width = maxWidth;
			} else {
				params.width = minWidth;
			}

			child.setLayoutParams(params);
		}
	}

	// 这里用handler更新界面
	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			if (msg.what == 1) {
				setCurrentViewParams();
			} 
		}

	};

	// 停止动画
	private void stopAnimation() {
		currentOpe = -1;
		currentView = null;
	}

	private void startAnimation() {

		if (currentView == null || currentOpe == -1) {
			Log.d(TAG, "无效动画");
			return;
		}
		
		animationIsEnd = false;
		onOffClickable();
		mHandler.sendEmptyMessage(1);
	}

	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		
		switch (id) {
		
		case R.id.btnOne:
			currentView = mainContain.getChildAt(0);
			break;
		case R.id.btnTwo:
			currentView = mainContain.getChildAt(1);
			break;
		case R.id.btnThree:
			currentView = mainContain.getChildAt(2);
			break;
		case R.id.btnFour:
			currentView = mainContain.getChildAt(3);
			break;
		}

		Log.i(TAG, ((Button) currentView).getText().toString() + " click");
		
		if (currentView != null && animationIsEnd) {
			
			int currentViewWidth = currentView.getWidth();
			
			if (currentViewWidth == maxWidth) {
				currentOpe = OPE_SMALL;
			} else {
				currentOpe = OPE_BIG;
			}
			
			clickEvent(currentView);
			
			startAnimation();
		}

	}

	private void clickEvent(View view) {
		View child;
		int childCount = mainContain.getChildCount();
		for (int i = 0; i < childCount; i++) {
			child = mainContain.getChildAt(i);
			if (preView == child) {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child
						.getLayoutParams();
				params.weight = 1.0f;
				child.setLayoutParams(params);
			} else {
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child
						.getLayoutParams();
				params.weight = 0.0f;
				params.width = minWidth;
				child.setLayoutParams(params);
			}
		}
		preView = view;
		printWeight();
	}

	private void printWeight() {
		View child;
		int childCount = mainContain.getChildCount();
		for (int i = 0; i < childCount; i++) {
			child = mainContain.getChildAt(i);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) child
					.getLayoutParams();
			Log.i("mm1", ((Button) child).getText() + ": " + params.weight);
		}
	}
	
	// 
	private void onOffClickable()
	{
		View child;
		boolean clickable = animationIsEnd;
		int childCount = mainContain.getChildCount();
		for (int i = 0; i < childCount; i++) {
			child = mainContain.getChildAt(i);
			child.setClickable(clickable);
		}
	}

}
