package com.example.dev.other.o4;

import android.app.Activity;
import android.os.Bundle;
import com.example.dev.R;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.other_o4_activity_second);
	}
}
