package com.example.dev.other.o5;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.dev.R;

public class ResultActivity extends Activity {

    private ImageView barcodeImageView;
    private TextView formatTextView;
    private TextView timeTextView;
    private TextView metaTextView;
    private TextView resultTextView;
    private Bitmap barcodeBitmap;
    private String barcodeFormat;
    private String decodeDate;
    private CharSequence metadataText;
    private String resultString;
    private Bundle bundle;
    private Button backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_o5_activity_result);
        initView();
        mGetIntentData();
        setView();

    }

    private void setView() {
        barcodeImageView.setImageBitmap(barcodeBitmap);
        formatTextView.setText(barcodeFormat);
        timeTextView.setText(decodeDate);
        metaTextView.setText(metadataText);
        resultTextView.setText(resultString);
    }

    private void mGetIntentData() {
        bundle = new Bundle();
        bundle = this.getIntent().getExtras();
        barcodeBitmap = bundle.getParcelable("bitmap");
        barcodeFormat = bundle.getString("barcodeFormat");
        decodeDate = bundle.getString("decodeDate");
        metadataText = bundle.getCharSequence("metadataText");
        resultString = bundle.getString("resultString");
    }

    private void initView() {
        barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
        formatTextView = (TextView) findViewById(R.id.format_text_view);
        timeTextView = (TextView) findViewById(R.id.time_text_view);
        metaTextView = (TextView) findViewById(R.id.meta_text_view);
        resultTextView = (TextView) findViewById(R.id.contents_text_view);
        backBtn = (Button) findViewById(R.id.mresult_button_back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
