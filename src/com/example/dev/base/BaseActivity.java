package com.example.dev.base;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by zn on 14-6-24.
 */
public abstract class BaseActivity extends Activity{

    public abstract void initView();
    public abstract void initData();
    public abstract void addListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
