package com.example.dev.animation.Shimmer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.example.dev.R;
import com.example.dev.animation_library.Shimmer.Shimmer;
import com.example.dev.animation_library.Shimmer.ShimmerTextView;

public class ShimmerActivity extends Activity {

    ShimmerTextView tv;
    Shimmer shimmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_shimmer_activity_main);

        tv = (ShimmerTextView) findViewById(R.id.shimmer_tv);
    }

    public void toggleAnimation(View target) {
        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(tv);
        }
    }
}
