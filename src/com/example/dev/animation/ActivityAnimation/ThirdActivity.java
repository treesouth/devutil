package com.example.dev.animation.ActivityAnimation;

import com.example.dev.R;

public class ThirdActivity extends AnimatedRectActivity {

    @Override
    protected int layoutResId() {
        return R.layout.animation_activityanimation_activity_third;
    }
}
