package com.example.dev.animation.ActivityAnimation;

import android.os.Bundle;
import com.example.dev.R;


public class DoorActivity extends AnimatedDoorActivity {

    @Override
    protected int layoutResId() {
        return R.layout.animation_activityanimation_activity_door;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
