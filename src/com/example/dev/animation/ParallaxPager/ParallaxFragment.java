package com.example.dev.animation.ParallaxPager;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.dev.R;
import com.example.dev.animation_library.ParallaxPager.ParallaxContainer;

public class ParallaxFragment extends Fragment {

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.animation_parallaxpager_fragment_parallax, container, false);


    // find the parallax container
    ParallaxContainer parallaxContainer =
        (ParallaxContainer) view.findViewById(R.id.parallax_container);

    // specify whether pager will loop
    parallaxContainer.setLooping(true);

    // wrap the inflater and inflate children with custom attributes
    parallaxContainer.setupChildren(inflater,
        R.layout.animation_parallaxpager_parallax_view_1,
        R.layout.animation_parallaxpager_parallax_view_2,
        R.layout.animation_parallaxpager_parallax_view_3,
        R.layout.animation_parallaxpager_parallax_view_4);

    view.findViewById(R.id.btn_new_frag).setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        getFragmentManager().beginTransaction()
            .replace(android.R.id.content, new Fragment())
            .addToBackStack("test")
            .commit();
      }
    });

    return view;
  }
}
