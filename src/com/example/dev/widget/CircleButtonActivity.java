package com.example.dev.widget;

import android.app.Activity;
import android.os.Bundle;
import com.example.dev.R;

/**
 * Created by zn on 14-6-27.
 */
public class CircleButtonActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_circlebutton_activity);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}