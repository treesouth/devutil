package com.example.dev.widget.RippleView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.dev.R;
import com.example.dev.widget_library.RippleView.RippleView;

public class RippleViewActivity extends Activity {

	RippleView mButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_rippleview_main);
        mButton = (RippleView) findViewById(R.id.btn);
        mButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Ripples Yo! :D", Toast.LENGTH_LONG).show();
			}
		});
    }

}
