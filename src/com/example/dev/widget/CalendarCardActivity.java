package com.example.dev.widget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.example.dev.R;

/**
 * Created by zn on 14-6-27.
 */
public class CalendarCardActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_calendarcard_activity);
    }
    public void handleSample1(View v) {
        startActivity(new Intent(this, CalendarCardActivity_1.class));
    }

    public void handleSample2(View v) {
        startActivity(new Intent(this, CalendarCardActivity_2.class));
    }

}