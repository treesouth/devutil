package com.example.dev.widget.L_Dialog;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.dev.R;

/**
 * Created by zn on 14-9-26.
 */
public class LdialogActivity extends FragmentActivity implements
		View.OnClickListener {
	private Button mBtn1, mBtn2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_ldialog_activity);
		mBtn1 = (Button) findViewById(R.id.btn1);
		mBtn2 = (Button) findViewById(R.id.btn2);
		mBtn1.setOnClickListener(this);
		mBtn2.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn1:
			CustomDialog.Builder builder = new CustomDialog.Builder(this,
					"Title", "OK");

			builder.content("This is a Android L Style Dialog...Do you love it ?");

//            builder.negativeText(String negativeText);
//            builder.darkTheme(boolean isDark);
//            builder.typeface(Typeface typeface);
//            builder.titleTextSize(int size);
//            builder.contentTextSize(int size);
//            builder.buttonTextSize(int size);
//            builder.titleAlignment(Alignment alignment); // Use either Alignment.LEFT, Alignment.CENTER or Alignment.RIGHT
//            builder.titleColor(String hex); // int res, or int colorRes parameter versions available as well.
//            builder.contentColor(String hex); // int res, or int colorRes parameter versions available as well.
//            builder.positiveColor(String hex); // int res, or int colorRes parameter versions available as well.
//            builder.negativeColor(String hex); // int res, or int colorRes parameter versions available as well.
//            builder.positiveBackground(Drawable drawable); // int res parameter version also available.
//            builder.rightToLeft(boolean rightToLeft); // Enables right to left positioning for languages that may require so.

			// Now we can build the dialog.
			CustomDialog customDialog = builder.build();

			customDialog.setClickListener(new CustomDialog.ClickListener() {
				@Override
				public void onConfirmClick() {
					Toast.makeText(LdialogActivity.this, "click Confirm",
							Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onCancelClick() {
					Toast.makeText(LdialogActivity.this, "click Cancel",
							Toast.LENGTH_SHORT).show();
				}
			});

			// Show the dialog.
			customDialog.show();
			break;
		case R.id.btn2:
			// To add a listview selector use the following code:

			// StateListDrawable selector = new StateListDrawable();
			// selector.addState(new int[]{android.R.attr.state_pressed}, new
			// ColorDrawable(R.color.color1));
			// selector.addState(new int[]{-android.R.attr.state_pressed}, new
			// ColorDrawable(R.color.color2));

			// The important part:
			// customListDialog.getListView().setSelector(selector);

			String[] ss = new String[] { "father", "mather", "wife", "me" };
			CustomListDialog.Builder builderList = new CustomListDialog.Builder(
					this, "Title", ss);
			// Now we can build the dialog.
			CustomListDialog customListDialog = builderList.build();

			customListDialog
					.setListClickListener(new CustomListDialog.ListClickListener() {
						@Override
						public void onListItemSelected(int i, String[] strings,
								String s) {
							// i is the position clicked.
							// strings is the array of items in the list.
							// s is the item selected.
							Toast.makeText(LdialogActivity.this, s,
									Toast.LENGTH_SHORT).show();
						}
					});
			// Show the dialog.
			customListDialog.show();

			break;
		}

	}
}
