package com.example.dev.widget.NumberProgressBar;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.example.dev.R;
import com.example.dev.widget_library.NumberProgressBar.NumberProgressBar;

import java.util.Timer;
import java.util.TimerTask;


public class NumberProgressBarActivity extends FragmentActivity {
    private int counter = 0;
    private Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_numberprogressbar_activity_main);

        final NumberProgressBar bnp = (NumberProgressBar)findViewById(R.id.numberbar1);
        counter = 0;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bnp.incrementProgressBy(1);
                        counter ++;
                        if (counter == 110) {
                            bnp.setProgress(0);
                            counter=0;
                        }
                    }
                });
            }
        }, 1000, 100);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
