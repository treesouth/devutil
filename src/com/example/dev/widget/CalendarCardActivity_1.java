package com.example.dev.widget;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.example.dev.R;
import com.example.dev.widget_library.CalendarCard.CalendarCard;
import com.example.dev.widget_library.CalendarCard.CardGridItem;
import com.example.dev.widget_library.CalendarCard.OnCellItemClick;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by zn on 14-6-27.
 */
public class CalendarCardActivity_1 extends Activity {

    private TextView mTextView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_calendarcard_activity_1);
        CalendarCard mCalendarCard = (CalendarCard) findViewById(R.id.calendarCard1);
        mTextView = (TextView) findViewById(R.id.textView1);

        mCalendarCard.setOnCellItemClick(new OnCellItemClick() {
            @Override
            public void onCellClick(View v, CardGridItem item) {
                mTextView.setText(getResources().getString(R.string.sel_date, new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(item.getDate().getTime())));
            }
        });

    }
}