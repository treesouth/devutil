package com.example.dev.widget;

import android.app.Activity;
import android.os.Bundle;
import com.example.dev.R;
import com.example.dev.widget_library.HoloColorPicker.*;

/**
 * Created by zn on 14-6-27.
 */
public class HoloColorPickerActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_holocolorpicker_activity);
        initView();
    }

    private void initView() {
        ColorPicker picker = (ColorPicker) findViewById(R.id.picker);
        SVBar svBar = (SVBar) findViewById(R.id.svbar);
        OpacityBar opacityBar = (OpacityBar) findViewById(R.id.opacitybar);
        SaturationBar saturationBar = (SaturationBar) findViewById(R.id.saturationbar);
        ValueBar valueBar = (ValueBar) findViewById(R.id.valuebar);

        picker.addSVBar(svBar);
        picker.addOpacityBar(opacityBar);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);

//To get the color
        picker.getColor();

//To set the old selected color u can do it like this
        picker.setOldCenterColor(picker.getColor());
// adds listener to the colorpicker which is implemented
//in the activity
//        picker.setOnColorChangedListener(this);

//to turn of showing the old color
        picker.setShowOldCenterColor(false);

//adding onChangeListeners to bars
//        opacityBar.setOnOpacityChangeListener(new OnOpacityChangeListener …)
//        valueBar.setOnValueChangeListener(new NumberPicker.OnValueChangeListener …)
//        saturationBar.setOnSaturationChangeListener(new OnSaturationChangeListener …)
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}