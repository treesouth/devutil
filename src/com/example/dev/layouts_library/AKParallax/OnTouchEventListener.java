package com.example.dev.layouts_library.AKParallax;

import android.view.MotionEvent;

/**
 * Created by mobile on 5/5/14.
 */
public interface OnTouchEventListener {
    void onTouchEvent(MotionEvent ev);
}
