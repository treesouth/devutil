package com.example.dev.layouts_library.WheelView.adapter;

import android.graphics.drawable.Drawable;

public interface WheelAdapter {
    Drawable getDrawable(int position);

    int getCount();
}
