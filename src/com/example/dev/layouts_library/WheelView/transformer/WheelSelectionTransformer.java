package com.example.dev.layouts_library.WheelView.transformer;

import android.graphics.drawable.Drawable;

import com.example.dev.layouts_library.WheelView.WheelView;

public interface WheelSelectionTransformer {
    void transform(Drawable drawable, WheelView.ItemState itemState);
}
