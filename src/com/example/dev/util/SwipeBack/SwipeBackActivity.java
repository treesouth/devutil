package com.example.dev.util.SwipeBack;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.example.dev.R;

public class SwipeBackActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.util_swipeback_activity_main);
	}
	
	public void jump(View view) {
		startActivity(new Intent(this, SecondActivity.class));
	}

}
