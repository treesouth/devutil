package com.example.dev.util.EditTextValidator;

import android.text.TextUtils;
import android.widget.EditText;

import com.example.dev.util_library.EditTextValidator.form.Validator;

public class CiaoValidator
        extends Validator {

    public CiaoValidator(String customErrorMessage) {
        super(customErrorMessage);

    }

    @Override
    public boolean isValid(EditText et) {
        return TextUtils.equals(et.getText(), "ciao");
    }

}