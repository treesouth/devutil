package com.example.dev.util.EditTextValidator;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.dev.R;
import com.example.dev.util.EditTextValidator.utils.LayoutListItem;
import com.example.dev.util.EditTextValidator.utils.ListItem;
import com.example.dev.util.EditTextValidator.utils.SimpleListItem;

public class EditTextValidatorActivity
        extends ListActivity
        implements OnItemClickListener {
    public EditTextValidatorActivity() {
        stringItems = new String[lItems.length];
        for (int i = 0; i < lItems.length; i++) {
            stringItems[i] = lItems[i].getListTitle();
            Log.d(stringItems[i], stringItems[i]);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView mLv = new ListView(this);
        mLv.setId(android.R.id.list);
        setContentView(mLv); // Don't try this at home :)

        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stringItems));
        getListView().setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.util_edittextvalidator_main, menu);
        return true;
    }

    public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
        lItems[pos].goToDemo(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.prefs:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                return false;
        }
    }

    private ListItem[] lItems = new ListItem[]{

            new LayoutListItem("Alpha", R.layout.util_edittextvalidator_alpha, R.string.explanation_alpha),
            new LayoutListItem("Person Name", R.layout.util_edittextvalidator_personname, R.string.explanation_personname),
            new LayoutListItem("Person Full Name", R.layout.util_edittextvalidator_personfullname, R.string.explanation_personfullname),
            new LayoutListItem("Date", R.layout.util_edittextvalidator_date, R.string.explanation_date),
            new LayoutListItem("Date Custom Format", R.layout.util_edittextvalidator_date_custom, R.string.explanation_date_custom),
            new LayoutListItem("Numeric only", R.layout.util_edittextvalidator_numeric, R.string.explanation_numeric),
            new LayoutListItem("Email", R.layout.util_edittextvalidator_email, R.string.explanation_email),
            new LayoutListItem("Credit Card Number", R.layout.util_edittextvalidator_creditcard, R.string.explanation_creditcard),
            new LayoutListItem("Phone", R.layout.util_edittextvalidator_phone, R.string.explanation_phone),
            new LayoutListItem("Domain Name", R.layout.util_edittextvalidator_domainname, R.string.explanation_domainname),
            new LayoutListItem("IP Address", R.layout.util_edittextvalidator_ipaddress, R.string.explanation_ipaddress),
            new LayoutListItem("WEB Url", R.layout.util_edittextvalidator_weburl, R.string.explanation_weburl),
            new LayoutListItem("Regexp", R.layout.util_edittextvalidator_regexp, R.string.explanation_regexp),
            new LayoutListItem("Emptyness (nocheck)", R.layout.util_edittextvalidator_nocheck, R.string.explanation_nocheck),
            new LayoutListItem("Custom Messages", R.layout.util_edittextvalidator_phone_custommessages, R.string.explanation_phone_custommmessages),
            new LayoutListItem("Allow Empty", R.layout.util_edittextvalidator_allowempty, R.string.explanation_allow_empty),
            new LayoutListItem("Programmatically Added Checks", R.layout.util_edittextvalidator_custom, R.string.explanation_programatic),
            new SimpleListItem("Email OR CreditCard", EmailOrCreditCard.class),
    };

    private final String[] stringItems;

}