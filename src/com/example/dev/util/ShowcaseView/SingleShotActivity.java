package com.example.dev.util.ShowcaseView;

import android.app.Activity;
import android.os.Bundle;

import com.example.dev.R;
import com.example.dev.util_library.ShowcaseView.ShowcaseView;
import com.example.dev.util_library.ShowcaseView.targets.Target;
import com.example.dev.util_library.ShowcaseView.targets.ViewTarget;

public class SingleShotActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.util_showcaseview_activity_single_shot);

        Target viewTarget = new ViewTarget(R.id.button, this);
        new ShowcaseView.Builder(this, true)
                .setTarget(viewTarget)
                .setContentTitle(R.string.title_single_shot)
                .setContentText(R.string.R_string_desc_single_shot)
                .singleShot(42)
                .build();
    }
}
