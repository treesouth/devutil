package com.example.dev.util.ShowcaseView.v14;

import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.example.dev.R;
import com.example.dev.util_library.ShowcaseView.ShowcaseView;
import com.example.dev.util_library.ShowcaseView.targets.ActionItemTarget;
import com.example.dev.util_library.ShowcaseView.targets.ActionViewTarget;

public class ActionItemsSampleActivity extends FragmentActivity {

    ShowcaseView sv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.util_showcaseview_activity_sample);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.util_showcaseview_main, menu);

        ActionViewTarget target = new ActionViewTarget(this, ActionViewTarget.Type.OVERFLOW);
        sv = new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle(R.string.showcase_simple_title)
                .setContentText(R.string.showcase_simple_message)
                .doNotBlockTouches()
                .build();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            ActionViewTarget target = new ActionViewTarget(this, ActionViewTarget.Type.HOME);
            sv.setShowcase(target, true);
        } else if (itemId == R.id.menu_item1) {
            ActionItemTarget target = new ActionItemTarget(this, R.id.menu_item1);
            sv.setShowcase(target, true);
        } else if (itemId == R.id.menu_item2) {
            ActionViewTarget target = new ActionViewTarget(this, ActionViewTarget.Type.TITLE);
            sv.setShowcase(target, true);
        }
        return true;
    }

}
