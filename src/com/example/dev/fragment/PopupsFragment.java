package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.popups.ActionSheet.ActionSheetActivity;
import com.example.dev.popups.AppMsg.AppMsgActivity;
import com.example.dev.popups.NiftyDialog.NiftyDialogActivity;
import com.example.dev.popups.StyledDialog.StyledDialogActivity;
import com.example.dev.popups_library.AppMsg.AppMsg;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-7-10.
 */
public class PopupsFragment extends ListFragment {
    public static final String TAG = PopupsFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.POPUPS));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, StyledDialogActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, AppMsgActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, NiftyDialogActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, ActionSheetActivity.class));
                break;
        }

    }
}