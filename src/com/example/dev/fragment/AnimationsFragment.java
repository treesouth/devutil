package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.animation.ActivityAnimation.ActivityAnimationActivity;
import com.example.dev.animation.DiscrollView.DiscrollViewActivity;
import com.example.dev.animation.EasingFunctions.EasingFunctionsActivity;
import com.example.dev.animation.FloatingActionButton.FloatingActionButtonActivity;
import com.example.dev.animation.ImageSlider.ImageSliderActivity;
import com.example.dev.animation.JazzyViewPager.JazzyViewPagerActivity;
import com.example.dev.animation.ListViewAnimations.ListViewAnimationsActivity;
import com.example.dev.animation.ParallaxPager.ParallaxActivity;
import com.example.dev.animation.Rebound.ReboundActivity;
import com.example.dev.animation.Shimmer.ShimmerActivity;
import com.example.dev.animation.SwipeCards.SwipeCardsActivity;
import com.example.dev.animation.ViewAnimation.ViewAnimationActivity;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-7-10.
 */
public class AnimationsFragment extends ListFragment {
    public static final String TAG = AnimationsFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.ANIMATION));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, DiscrollViewActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, ShimmerActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, ImageSliderActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, ListViewAnimationsActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, SwipeCardsActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, ActivityAnimationActivity.class));
                break;
            case 6:
                startActivity(new Intent(mActivity, JazzyViewPagerActivity.class));
                break;
            case 7:
                startActivity(new Intent(mActivity, FloatingActionButtonActivity.class));
                break;
            case 8:
                startActivity(new Intent(mActivity, ReboundActivity.class));
                break;
            case 9:
                startActivity(new Intent(mActivity, EasingFunctionsActivity.class));
                break;
            case 10:
                startActivity(new Intent(mActivity, ParallaxActivity.class));
                break;
            case 11:
                startActivity(new Intent(mActivity, ViewAnimationActivity.class));
                break;
        }

    }
}