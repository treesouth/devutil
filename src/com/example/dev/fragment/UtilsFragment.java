package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.util.AppRater.AppRaterActivity;
import com.example.dev.util.EditTextValidator.EditTextValidatorActivity;
import com.example.dev.util.FloatHint.FloatHintActivity;
import com.example.dev.util.ShowcaseView.ShowcaseViewActivity;
import com.example.dev.util.SwipeBack.SwipeBackActivity;
import com.example.dev.util.WizardPager.WizardPagerActivity;
import com.example.dev.utils.TitleUtil;
import com.example.dev.widget.TimelyTextView.TimelyTextViewActivity;

/**
 * Created by zn on 14-7-10.
 */
public class UtilsFragment extends ListFragment {
    public static final String TAG = UtilsFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.UTILS));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, EditTextValidatorActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, ShowcaseViewActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, WizardPagerActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, FloatHintActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, AppRaterActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, SwipeBackActivity.class));
                break;

        }

    }
}