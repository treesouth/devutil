package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.graphics.BlurEffect.BlurEffectActivity;
import com.example.dev.graphics.CrooperActivity;
import com.example.dev.graphics.CustomShapeImageViewActivity;
import com.example.dev.graphics.GifDrawable.GifDrawableActivity;
import com.example.dev.graphics.GpuImage.activity.GpuImageActivity;
import com.example.dev.graphics.KenBurnsView.KenBurnsViewActivity;
import com.example.dev.graphics.PhotoView.PhotoViewActivity;
import com.example.dev.graphics.SystemBarTint.SystemBarTintActivity;
import com.example.dev.graphics.TouchImageView.TouchImageViewActivity;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-7-10.
 */
public class GraphicsFragment extends ListFragment {
    public static final String TAG = GraphicsFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.GRAPHIC));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, CrooperActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, PhotoViewActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, GifDrawableActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, CustomShapeImageViewActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, KenBurnsViewActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, BlurEffectActivity.class));
                break;
            case 6:
                startActivity(new Intent(mActivity, GpuImageActivity.class));
                break;
            case 7:
                startActivity(new Intent(mActivity, SystemBarTintActivity.class));
                break;
            case 8:
                startActivity(new Intent(mActivity, TouchImageViewActivity.class));
                break;
        }

    }
}