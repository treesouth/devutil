package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.layouts.AKParallax.AKParallaxActivity;
import com.example.dev.layouts.FlatUI.FlatUIActivity;
import com.example.dev.layouts.FoldableLayout.activities.FoldableLayoutActivity;
import com.example.dev.layouts.FreeFlow.ArtBook.ArtbookActivity;
import com.example.dev.layouts.FreeFlow.PhotoGrid.PhotoGridActivity;
import com.example.dev.layouts.GuideBackground.GuideBackgroundActivity;
import com.example.dev.layouts.PullToZoom.PullToZoomActivity;
import com.example.dev.layouts.Smoothie.SmoothieActivity;
import com.example.dev.layouts.StaggeredGrid.StaggeredGridMainActivity;
import com.example.dev.layouts.VideoView.VideoViewActivity;
import com.example.dev.layouts.WheelView.WheelViewActivity;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-7-10.
 */
public class LayoutsFragment extends ListFragment {
    public static final String TAG = LayoutsFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.LAYOUTS));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, AKParallaxActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, GuideBackgroundActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, FoldableLayoutActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, FlatUIActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, PhotoGridActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, ArtbookActivity.class));
                break;
            case 6:
                startActivity(new Intent(mActivity, StaggeredGridMainActivity.class));
                break;
            case 7:
                startActivity(new Intent(mActivity, VideoViewActivity.class));
                break;
            case 8:
                startActivity(new Intent(mActivity, SmoothieActivity.class));
                break;
            case 9:
                startActivity(new Intent(mActivity, WheelViewActivity.class));
                break;
            case 10:
                startActivity(new Intent(mActivity, PullToZoomActivity.class));
                break;
        }

    }
}