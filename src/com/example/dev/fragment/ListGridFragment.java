package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.listgrid.AnimatedExpandableListViewActivity;
import com.example.dev.listgrid.EnhancedListViewActivity;
import com.example.dev.listgrid.JazzyListView.JazzyListViewActivity;
import com.example.dev.listgrid.ListViewFilter.ui.ListViewFilterActivity;
import com.example.dev.listgrid.PinnedSectionListViewActivity;
import com.example.dev.listgrid.StickyGridHeader.StickyGridHeaderActivity;
import com.example.dev.listgrid.StickyListHeader.StickyListHeaderActivity;
import com.example.dev.listgrid.SwipeListView.SwipeListViewActivity;
import com.example.dev.listgrid.SwipeRefreshLayout.SwipeRefreshLayoutActivity;
import com.example.dev.listgrid.ZrcListView.ZrcListViewActivity;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-6-25.
 */
public class ListGridFragment extends ListFragment {
    public static final String TAG = ListGridFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.LISTGRID));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, EnhancedListViewActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, StickyListHeaderActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, PinnedSectionListViewActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, JazzyListViewActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, ListViewFilterActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, AnimatedExpandableListViewActivity.class));
                break;
            case 6:
                startActivity(new Intent(mActivity, ZrcListViewActivity.class));
                break;
            case 7:
                startActivity(new Intent(mActivity, SwipeRefreshLayoutActivity.class));
                break;
            case 8:
                startActivity(new Intent(mActivity, SwipeListViewActivity.class));
                break;
            case 9:
                startActivity(new Intent(mActivity, StickyGridHeaderActivity.class));
                break;
        }

    }
}