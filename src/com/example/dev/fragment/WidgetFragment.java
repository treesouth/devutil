package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.MainActivity;
import com.example.dev.Const;
import com.example.dev.R;
import com.example.dev.utils.TitleUtil;
import com.example.dev.widget.AutoFitTextView.AutoFitTextViewActivity;
import com.example.dev.widget.CalendarCardActivity;
import com.example.dev.widget.CircleButtonActivity;
import com.example.dev.widget.CircleProgressButton.CircleProgressButtonActivity;
import com.example.dev.widget.HoloColorPickerActivity;
import com.example.dev.widget.JumpingBeans.JumpingBeansActivity;
import com.example.dev.widget.L_Dialog.LdialogActivity;
import com.example.dev.widget.NumberProgressBar.NumberProgressBarActivity;
import com.example.dev.widget.ProcessButton.ProcessButtonActivity;
import com.example.dev.widget.ProgressButton.ProgressButtonActivity;
import com.example.dev.widget.RippleView.RippleViewActivity;
import com.example.dev.widget.SeekArc.SeekArcActivity;
import com.example.dev.widget.SwitchButton.SwitchButtonActivity;
import com.example.dev.widget.TimelyTextView.TimelyTextViewActivity;


/**
 * Created by zn on 14-6-25.
 */
public class WidgetFragment extends ListFragment {
    public static final String TAG = WidgetFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.WIDGET));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, HoloColorPickerActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, CircleButtonActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, CalendarCardActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, CircleProgressButtonActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, ProcessButtonActivity.class));
                break;
            case 5:
                startActivity(new Intent(mActivity, AutoFitTextViewActivity.class));
                break;
            case 6:
                startActivity(new Intent(mActivity, TimelyTextViewActivity.class));
                break;
            case 7:
                startActivity(new Intent(mActivity, SwitchButtonActivity.class));
                break;
            case 8:
                startActivity(new Intent(mActivity, NumberProgressBarActivity.class));
                break;
            case 9:
                startActivity(new Intent(mActivity, JumpingBeansActivity.class));
                break;
            case 10:
                startActivity(new Intent(mActivity, SeekArcActivity.class));
                break;
            case 11:
                startActivity(new Intent(mActivity, ProgressButtonActivity.class));
                break;
            case 12:
                startActivity(new Intent(mActivity, RippleViewActivity.class));
                break;
            case 13:
                startActivity(new Intent(mActivity, LdialogActivity.class));
                break;
        }


    }
}