package com.example.dev.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.dev.Const;
import com.example.dev.MainActivity;
import com.example.dev.R;
import com.example.dev.other.o1.PinchActivity;
import com.example.dev.other.o2.FlowerActivity;
import com.example.dev.other.o3.TimelineActivity;
import com.example.dev.other.o4.SwitchLanguageActivity;
import com.example.dev.other.o5.CaptureActivity;
import com.example.dev.tabs.PagerSlidingTabStrip.PagerSlidingTabStripActivity;
import com.example.dev.utils.TitleUtil;

/**
 * Created by zn on 14-7-10.
 */
public class OthersFragment extends ListFragment {
    public static final String TAG = OthersFragment.class.getSimpleName();

    private MainActivity mActivity;
    private ArrayAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listfragment, container, false);
        initView();
        return rootView;
    }

    private void initView() {
        mAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_list_item_1, TitleUtil.addTitleList(mActivity, Const.OTHER));
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(mActivity, PinchActivity.class));
                break;
            case 1:
                startActivity(new Intent(mActivity, FlowerActivity.class));
                break;
            case 2:
                startActivity(new Intent(mActivity, TimelineActivity.class));
                break;
            case 3:
                startActivity(new Intent(mActivity, SwitchLanguageActivity.class));
                break;
            case 4:
                startActivity(new Intent(mActivity, CaptureActivity.class));
                break;
        }

    }
}